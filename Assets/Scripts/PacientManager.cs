﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class PacientManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameController gameCtrl;

    public Image healtyLungs;

    public Image sickLungs;

    public Sequence sequence;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }


    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(string _HealtyLungChildName = "Image_Lungs_Healty",
                                   string _SickLungChildName = "Image_Lungs_Sick")
    {
        foreach(Transform child in transform)
        {
            if(child.name == _HealtyLungChildName)
            {
                healtyLungs = child.GetComponent<Image>();
            }

            if (child.name == _SickLungChildName)
            {
                sickLungs = child.GetComponent<Image>();
            }
        }
    }

    public void Initialize()
    {
        sequence = DOTween.Sequence()
                          .Append(sickLungs.DOFillAmount(1, gameCtrl.countdownDuration));
    }


    public void Play()
    {
        sequence.Play();
    }

    public void Pause()
    {
        sequence.Pause();
    }

    public void UpdateDuration(float duration)
    {
        if (sequence.IsPlaying()) 
            sequence.Kill();
        
        sequence = DOTween.Sequence()
                          .Append(sickLungs.DOFillAmount(1, duration))
                          .Pause();
    }

    #endregion

}
