﻿using System;
using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class CountdownManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameController gameCtrl;

    public Image empty;
    public Image fill;

    public Sequence sequence;
    public float strength = 2.5f;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }



    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(string _EmptyChildName = "Image_Empty",
                                   string _FillChildName = "Image_Fill")
    {
        foreach (Transform child in transform)
        {
            if (child.name == _EmptyChildName)
            {
                empty = child.GetComponent<Image>();
            }

            if (child.name == _FillChildName)
            {
                fill = child.GetComponent<Image>();
            }
        }
    }

    public void Initialize()
    {
        if (sequence != null && sequence.IsPlaying()) sequence.Kill();

        sequence = DOTween.Sequence()
                          .Append(fill.DOFillAmount(0, gameCtrl.countdownDuration * fill.fillAmount))
                          .OnComplete(() => gameCtrl.Notify_CountdownFinished());
    }

    public void Shake()
    {
        transform.DOShakePosition(gameCtrl.baseDuration / 2, 
                                  new Vector3().With(x: strength, y: strength));
    }

    public void Play()
    {
        sequence.Play();
    }

    public void Pause()
    {
        sequence.Pause();
    }

    public void AddBonusTime()
    {
        

        var newFill = Mathf.Clamp01(fill.fillAmount + 
                                    1.0f / gameCtrl.stopwatch.questions.Count);

        DOTween.Sequence()
               .Append(fill.DOFillAmount(newFill, gameCtrl.baseDuration / 2)
                           .SetEase(Ease.InOutBack));

        gameCtrl.Notify_CountdownFillUpdate(newFill);

        sequence = DOTween.Sequence()
                          .Append(fill.DOFillAmount(0, gameCtrl.countdownDuration * fill.fillAmount))
                          .OnComplete(() => gameCtrl.Notify_CountdownFinished());

        Initialize();
        Pause();
    }

    #endregion

}
