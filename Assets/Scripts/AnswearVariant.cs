﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Quizz.Miscellaneous;
using DG.Tweening;
using System;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class AnswearVariant : MonoBehaviour {

    #region Nested Types
        
	

    #endregion

    #region Fields and Properties

    public QuestionManager questionManager;

    public CanvasGroup canvasGroup;

    public Graphic variantBox;

    public Graphic variantContent;

    public SuperState currentState =
        new SuperState(InteractionState.Unselected,
                       ValidityState.Uncertain);

    public ValidityState targetValidity = ValidityState.Uncertain;

    public UnityEvent OnVariantSelected;

    public UnityEvent OnVariantDeselected;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(QuestionManager _QuestionManager = null,
                                   string _VariantBoxChildName = "Image_VariantBox",
                                   string _AnswearContentChildName = "Text_Answear")
    {
        if(_QuestionManager != null)
        {
            questionManager = _QuestionManager;
        }

        canvasGroup = GetComponent<CanvasGroup>();

        foreach(Transform child in transform)
        {
            if(child.name == _VariantBoxChildName)
            {
                variantBox = child.GetComponent<Graphic>();
            }

            if (child.name == _AnswearContentChildName)
            {
                variantContent = child.GetComponent<Graphic>();
            }
        }
    }

    public void OnAnswearVariant_Click()
    {
        //print(transform.name);
        if(targetValidity != ValidityState.Uncertain && !questionManager.isLocked)
        {
            TransitionTo(currentState.interactionState == InteractionState.Selected ?
                            currentState.With(InteractionState.Unselected) :
                            currentState.With(InteractionState.Selected));
            if (currentState.interactionState == InteractionState.Selected)
            {
                OnVariantSelected.Invoke();
            }
            else
            {
                OnVariantDeselected.Invoke();
            }
        }
    }

    public void Reset()
    {
        TransitionTo(
            new SuperState(InteractionState.Unselected,
                           ValidityState.Uncertain));
    }

    public void TransitionTo(SuperState _NewState)
    {
        
        if (currentState != _NewState) 
        {
            //print(_NewState.interactionState.ToString());
            currentState = _NewState;

            if(Application.isPlaying)
            {
                TransitionTo_WithTween(questionManager.specs[currentState]);
            }
            else
            {
                TransitionTo_WithSnap(questionManager.specs[currentState]);
            }
        }
    }

    void TransitionTo_WithTween(TransitionSpecs specs)
    {
        var duration = questionManager.baseDuration * specs.durationMultiplier;

        CheckIfTweeningAndComplete(canvasGroup);
        canvasGroup.DOFade(specs.canvasGroupAlpha, duration)
                   .SetLoops(specs.loopCount);

        CheckIfTweeningAndComplete(variantBox);
        variantBox.DOColor(specs.variantBoxColor, duration)
                  .SetLoops(specs.loopCount);

        CheckIfTweeningAndComplete(variantContent);
        variantContent.DOColor(specs.variantContentColor, duration)
                      .SetLoops(specs.loopCount);

    }

    void TransitionTo_WithSnap(TransitionSpecs specs)
    {
        CheckIfTweeningAndComplete(canvasGroup);
        canvasGroup.alpha = specs.canvasGroupAlpha;

        CheckIfTweeningAndComplete(variantBox);
        variantBox.color = specs.variantBoxColor;

        CheckIfTweeningAndComplete(variantContent);
        variantContent.color = specs.variantContentColor;

    }

    private void CheckIfTweeningAndComplete(object target)
    {
        if (DOTween.IsTweening(target))
        {
            DOTween.Complete(target);
        }
    }

    public bool ResolveValidity()
    {
        if (targetValidity == ValidityState.Uncertain)
            return true;

        TransitionTo(currentState.With( _ValidityState : targetValidity));

        return (currentState == new SuperState(InteractionState.Selected, ValidityState.Right)) ||
            (currentState == new SuperState(InteractionState.Unselected, ValidityState.Wrong));
        
    }



    #endregion

}
