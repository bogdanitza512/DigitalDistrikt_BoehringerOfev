﻿using System;
using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using TMPro;
using UnityEngine;
using DG.Tweening;
using ScreenMgr;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System.Linq;

/// <summary>
/// 
/// </summary>
public class StopwatchManager : SerializedMonoBehaviour {


    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameController gameCtrl;

    public TMP_Text quoteText;
    public List<string> quotes =
        new List<string>();

    public ScreenManager quizzManager;
    public List<BaseScreen> questions =
        new List<BaseScreen>();

    public Button stopwatchButton
;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }



    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(string _QuoteTextChildName = "Text_Quote",
                                   string _QuizzManagerChildName = "[QuizzManager]")
    {
        stopwatchButton = GetComponent<Button>();

        foreach(Transform child in transform)
        {
            if(child.name == _QuoteTextChildName)
            {
                quoteText = child.GetComponent<TMP_Text>();
            }

            if(child.name == _QuizzManagerChildName)
            {
                quizzManager = child.GetComponent<ScreenManager>();
            }
        }

        if(quizzManager != null)
        {
            foreach(Transform child in quizzManager.transform)
            {
                if(child.GetComponent<QuestionManager>())
                {
                    questions.Add(child.GetComponent<BaseScreen>());
                    child.GetComponent<QuestionManager>().AutoPopulateFields(gameCtrl);
                }
            }
        }
    }

    public bool IsQuizzComplete()
    {
        return whitelist.Count == questions.Count;
    }

    public void Initialize()
    {
        LoadRandomQuote();
        //StartPulsatingInSize();
        HideAllQuestions();
        MakeInteractable();
    }

    private void MakeInteractable()
    {
        stopwatchButton.interactable = true;
    }

    public void HideAllQuestions()
    {
        LoadRandomQuote();
        quizzManager.HideAll();
        stopwatchButton.interactable = true;
    }

    float pulseDuration = 2.0f;
    private void StartPulsatingInSize()
    {
        transform.DOScale(transform.localScale * 1.17f, pulseDuration);
    }

    public Queue<string> lastDisplayedQuotes = 
        new Queue<string>();
    [Button(ButtonSizes.Medium)]
    private void LoadRandomQuote()
    {
        var nextQuote = quotes.Where((q) => !lastDisplayedQuotes.Contains(q))
                              .ToList()
                              .GetRandomItem();
        quoteText.text = nextQuote;
        lastDisplayedQuotes.Enqueue(nextQuote);
        if (lastDisplayedQuotes.Count == quotes.Count)
        {
            lastDisplayedQuotes.Dequeue();
        }
    }

    public void OnStopwatch_Click()
    {
        stopwatchButton.interactable = false;
        LoadRandomQuestion();
    }

    public void LoadRandomQuestion()
    {
        currentQuestion = questions.Where((q) => !whitelist.Contains(q))
                                   .ToList()
                                   .GetRandomItem();
        quizzManager.ShowScreen(currentQuestion);
        gameCtrl.Notify_QuestionLoaded();
    }

    public List<BaseScreen> whitelist = 
        new List<BaseScreen>();
    public BaseScreen currentQuestion;

    public void WhitelistCurrentQuestion()
    {
        whitelist.Add(currentQuestion);
    }


    #endregion

}
