﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Quizz.Miscellaneous
{

    public enum InteractionState { Unselected, Selected }
    public enum ValidityState { Uncertain, Right, Wrong }

    [System.Serializable]
    public struct SuperState
    {

        public InteractionState interactionState;
        public ValidityState validityState;

        public SuperState(InteractionState _InteractionState,
                          ValidityState _ValidityState)
        {
            interactionState = _InteractionState;
            validityState = _ValidityState;
        }

        public SuperState With(InteractionState? _InteractionState = null,
                               ValidityState? _ValidityState = null)
        {
            InteractionState newInteractionState =
                _InteractionState ?? interactionState;
            ValidityState newValidityState =
                _ValidityState ?? validityState;

            return new SuperState(newInteractionState, newValidityState);
        }


        public static bool operator ==(SuperState state01, SuperState state02)
        {
            return (state01.interactionState == state02.interactionState &&
                    state01.validityState == state02.validityState);
        }

        public static bool operator !=(SuperState state01, SuperState state02)
        {
            return (state01.interactionState != state02.interactionState ||
                    state01.validityState != state02.validityState);
        }

    }

    public struct TransitionSpecs
    {

        public float canvasGroupAlpha;

        public Color variantBoxColor;
        public Color variantContentColor;

        public float durationMultiplier;
        public int loopCount;

        public TransitionSpecs(float _CanvasGroupAlpha,
                               Color _VariantBoxColor,
                               Color _AnswearContentColor,
                               float _DurationMultiplier,
                               int _LoopCount)
        {
            canvasGroupAlpha = _CanvasGroupAlpha;

            variantBoxColor = _VariantBoxColor;
            variantContentColor = _AnswearContentColor;

            durationMultiplier = _DurationMultiplier;
            loopCount = _LoopCount;
        }

    }


}



