﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScreenMgr;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class GameController : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    public ScreenManager masterScreenManager;

    public BaseScreen endScreen;

    public CountdownManager countdown;

    public StopwatchManager stopwatch;

    public PacientManager pacientManager;



    public float baseDuration = 1.0f;

    public  float countdownDuration = 30.0f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }



    #endregion

    #region Methods

    public void StartGame()
    {
        stopwatch.Initialize();
        countdown.Initialize();
        pacientManager.Initialize();
    }

    public void EndGame()
    {
        masterScreenManager.ShowScreen(endScreen);
    }

    public void RestartGame()
    {
        gameMode.RestartScene();
    }

    public void Notify_AnswearComputed(bool overallValidity)
    {
        
        stopwatch.WhitelistCurrentQuestion();

        if(overallValidity == true)
        {
            countdown.AddBonusTime();
        }
        else
        {
            countdown.Shake();
        }
    }

    public void Notify_QuestionAnsweard()
    {
        if(stopwatch.IsQuizzComplete())
        {
            EndGame();
        }
        else
        {
            stopwatch.HideAllQuestions();
            countdown.Play();
            pacientManager.Play();
        }
    }

    public void Notify_QuestionLoaded()
    {
        countdown.Pause();
        pacientManager.Pause();
    }

    public void Notify_CountdownFinished()
    {
        EndGame();
    }

    public void Notify_CountdownFillUpdate(float newFill)
    {
        pacientManager
            .UpdateDuration(countdownDuration * newFill);
    }

    #endregion

}
