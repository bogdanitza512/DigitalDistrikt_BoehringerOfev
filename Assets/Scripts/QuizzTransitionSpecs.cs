﻿using System.Collections;
using System.Collections.Generic;
using Quizz.Miscellaneous;
using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// 
/// </summary>
[CreateAssetMenu(fileName = "QTS_", menuName = "Custom/QuizzTransitionSpecs")]
public class QuizzTransitionSpecs : SerializedScriptableObject
{

    #region Nested Types



    #endregion

    #region Fields and Properties

    public Dictionary<SuperState, TransitionSpecs> transitionDict =
        new Dictionary<SuperState, TransitionSpecs>();

    public TransitionSpecs this[SuperState key]
    {
        get
        {
            return transitionDict[key];
        }
        set
        {
            transitionDict[key] = value;
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {

    }

    #endregion

    #region Methods



    #endregion

}