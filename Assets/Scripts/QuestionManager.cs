﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quizz.Miscellaneous;
using Sirenix.OdinInspector;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class QuestionManager : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameController gameCtrl;

    public QuizzTransitionSpecs specs;

    public float baseDuration = 1.0f;

    public List<AnswearVariant> answearVariants;
    public bool isLocked = false;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(GameController _GameCtrl = null)
    {

        if(_GameCtrl != null)
        {
            gameCtrl = _GameCtrl;
        }

        answearVariants.Clear();
        foreach(Transform child in transform)
        {
            var ans = child.GetComponent<AnswearVariant>();
            if(ans != null)
            {
                answearVariants.Add(ans);
                ans.AutoPopulateFields(this);
            }
        }
    }

    public void OnSendAnswear_Click()
    {
        if (isLocked) return;

        bool overallValidity = true;
        var duration = baseDuration;

        foreach (var answearVariant in answearVariants)
        {
            overallValidity &= answearVariant.ResolveValidity();
            duration += overallValidity ? baseDuration * .25f : baseDuration * .50f;
        }

        DOTween.Sequence()
               .AppendInterval(duration)
               .OnStart(() => gameCtrl.Notify_AnswearComputed(overallValidity))
               .OnComplete(() => gameCtrl.Notify_QuestionAnsweard());
        isLocked = true;

    }

    public void ResetQuestionScreen()
    {
        isLocked = false;
        foreach(var variant in answearVariants)
        {
            variant.Reset();
        }
    }

    #endregion

}
