﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class SubscriptionManager : MonoBehaviour {

    #region Nested Types

    [System.Serializable]
    public struct ConsumerData
    {
        public List<string> emails;
    }

    #endregion

    #region Fields and Properties

    public CanvasGroup canvasGroup;

    public string fileName;

    public ConsumerData data;

    public TMP_InputField inputField;

    public string currentEmail;

    VirtualKeyboard vk = new VirtualKeyboard();


    public float fadeDuration;


    string PathToDataInJSON
    {
        get { return Path.Combine(Application.streamingAssetsPath, fileName + ".json"); }
    }


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void AutoPopulateFields(string _InputFieldChildName = "Input_Email")
    {
        canvasGroup = GetComponent<CanvasGroup>();

        foreach(Transform child in transform)
        {
            if(child.name == _InputFieldChildName)
            {
                inputField = child.GetComponent<TMP_InputField>();
            }
        }
    }

    public void OnInputField_Selected()
    {
        vk.ShowTouchKeyboard();
    }

    public void OnInputField_Deselected()
    {
        vk.HideTouchKeyboard();
    }
	
    public void OnInputField_EndEdit(string email)
    {
        currentEmail = email;
    }

    public void OnSaveButton_Click()
    {
        Deserialize();
        data.emails.Add(currentEmail);
        Serialize();

        canvasGroup.DOFade(0, fadeDuration);
    }

    [Button(ButtonSizes.Medium)]
    public void Deserialize()
    {
        if (File.Exists(PathToDataInJSON))
        {
            string dataAsJSON = File.ReadAllText(PathToDataInJSON);
            JsonUtility.FromJsonOverwrite(dataAsJSON, data);
        }

    }

    [Button(ButtonSizes.Medium)]
    public void Serialize()
    {
        string dataAsJSON = JsonUtility.ToJson(data, true);
        File.WriteAllText(PathToDataInJSON, dataAsJSON);

    }


    #endregion

}
